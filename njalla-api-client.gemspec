# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = 'njalla-api-client'
  spec.version       = '0.2.0'
  spec.authors       = %w[f]
  spec.email         = %w[f@sutty.nl]

  spec.summary       = 'Njalla API Client'
  spec.description   = 'An API client for Njalla (https://njal.la/)'
  spec.homepage      = "https://0xacab.org/sutty/#{spec.name}"
  spec.license       = 'Apache-2.0'

  spec.required_ruby_version = Gem::Requirement.new('>= 2.7')

  spec.metadata = {
    'bug_tracker_uri' => "#{spec.homepage}/issues",
    'homepage_uri' => spec.homepage,
    'source_code_uri' => spec.homepage,
    'changelog_uri' => "#{spec.homepage}/-/blob/master/CHANGELOG.md",
    'documentation_uri' => "https://rubydoc.info/gems/#{spec.name}"
  }

  spec.files         = Dir['lib/**/*']
  spec.require_paths = %w[lib]

  spec.extra_rdoc_files = Dir['README.md', 'CHANGELOG.md', 'LICENSE']
  spec.rdoc_options += [
    '--title', "#{spec.name} - #{spec.summary}",
    '--main', 'README.md',
    '--line-numbers',
    '--inline-source',
    '--quiet'
  ]

  spec.add_runtime_dependency 'dry-schema'
  spec.add_runtime_dependency 'httparty', '~> 0.18'

  spec.add_development_dependency 'pry'
  spec.add_development_dependency 'rspec', '~> 3.6', '>= 3.6.0'
  spec.add_development_dependency 'rspec-tap-formatters'
  spec.add_development_dependency 'yard'

  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'webmock', '~> 1.24', '>= 1.24.3'
end

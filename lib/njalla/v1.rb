# frozen_string_literal: true

require_relative 'v1/client'
require_relative 'v1/domain'
require_relative 'v1/schemas'

module Njalla
  module V1
  end
end

# frozen_string_literal: true

require 'httparty'

module Njalla
  module V1
    # Implements HTTP client
    class Client
      include HTTParty

      base_uri 'https://njal.la/api/1'

      # @param token [String]
      def initialize(token:)
        @token = token
      end

      # @param method [String]
      # @param params [Dry::Schema::Result]
      def get(method, params = nil)
        raise params.errors.inspect unless params&.success?

        self.class.get('/', headers: headers, body: {
          jsonrpc: '2.0',
          method: method,
          params: params&.to_h
        }.to_json)
      end

      # @param method [String]
      # @param params [Dry::Schema::Result]
      def post(method, params)
        raise params.errors.inspect unless params.success?

        self.class.post('/', headers: headers, body: {
          jsonrpc: '2.0',
          method: method,
          params: params.to_h
        }.to_json)
      end

      private

      # @return [Hash]
      def headers
        @headers ||= {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: "Njalla #{@token}"
        }
      end
    end
  end
end

# frozen_string_literal: true

require_relative 'schemas/add_record'
require_relative 'schemas/remove_record'
require_relative 'schemas/record'

module Njalla
  module V1
    # Implement domain method APIv1
    class Domain
      # @return [Njalla::V1::Client]
      attr_reader :client
      # @return [String]
      attr_reader :domain

      # @param client [Njalla::V1::Client]
      # @param domain [String]
      def initialize(client:, domain:)
        @client = client
        @domain = domain
      end

      # Adds a record
      #
      # @param content [String]
      # @param name [String]
      # @param type [String]
      # @param ttl [Integer]
      # @return [Njalla::V1::Schemas::Record]
      def add_record(content:, name: '@', ttl: 60, type: 'A')
        record = Schemas::AddRecord.new.call(domain: domain, content: content, name: name, type: type, ttl: ttl)

        Schemas::Record.new.call(**client.post('add-record', record).dig('result'))
      end

      # Remove a record by its ID
      #
      # @param :id [String,Integer]
      # @return [Boolean]
      def remove_record(id:)
        record = Schemas::RemoveRecord.new.call(domain: domain, id: id)
        result = client.post('remove-record', record).dig('result')

        !result.nil? && result.empty?
      end
    end
  end
end

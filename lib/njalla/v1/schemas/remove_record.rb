# frozen_string_literal: true

require 'dry-schema'

module Njalla
  module V1
    module Schemas
      class RemoveRecord < Dry::Schema::JSON
        define do
          required(:id).filled(:integer)
          required(:domain).filled(:string)
        end
      end
    end
  end
end

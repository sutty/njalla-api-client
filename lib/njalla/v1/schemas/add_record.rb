# frozen_string_literal: true

require 'dry-schema'

module Njalla
  module V1
    module Schemas
      class AddRecord < Dry::Schema::JSON
        define do
          required(:domain).filled(:string)
          required(:name).filled(:string)
          required(:type).filled(:string)
          required(:ttl).filled(:integer)
          required(:content).filled(:string)
        end
      end
    end
  end
end

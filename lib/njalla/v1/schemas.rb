# frozen_string_literal: true

require_relative 'schemas/add_record'
require_relative 'schemas/record'
